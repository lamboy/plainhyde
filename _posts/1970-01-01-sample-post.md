---
layout: post
title: Sample Post
color:
  primary: magenta
  secondary: "#f9f"
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus interdum,
quam sed pretium pretium, eros lorem ultricies quam, non aliquet lacus quam id
quam. Donec varius hendrerit nisi et porta. Nulla consequat sagittis blandit.
Sed vitae lobortis neque, non vulputate urna. Vestibulum semper hendrerit nisi
eu ornare. Phasellus sit amet dolor sit amet purus convallis tempor. Fusce nec
semper purus, consectetur tincidunt dolor.

Aenean pretium dui nisi, sit amet tristique diam semper id. Curabitur dapibus,
libero sit amet ornare tincidunt, lorem odio varius diam, in faucibus nulla mi
vel quam. Etiam condimentum libero nec elit tincidunt auctor. Maecenas pulvinar
nisi et suscipit fringilla. Donec vehicula eget nibh in varius. Praesent vel
scelerisque nisl, vitae accumsan lectus. Pellentesque habitant morbi tristique
senectus et netus et malesuada fames ac turpis egestas. Donec eu convallis nisi.
Orci varius natoque penatibus et magnis dis parturient montes, nascetur
ridiculus mus. Nullam id consequat tortor. In hac habitasse platea dictumst.
Nulla ut fermentum dolor, eu mattis magna. Sed nisl libero, pharetra consectetur
faucibus in, consequat sit amet justo. Aenean dictum mattis enim aliquam porta.

Nam dictum lorem elit, feugiat dictum nibh tincidunt eu. Integer vel odio eget
orci cursus lacinia ac sit amet ligula. Lorem ipsum dolor sit amet, consectetur
adipiscing elit. Nullam fringilla sem in sapien vestibulum aliquam. Suspendisse
pulvinar et metus vel suscipit. Donec luctus, sem in suscipit convallis, mauris
augue eleifend lectus, in egestas nunc magna condimentum risus. Phasellus
interdum lectus ac dignissim rutrum. Nullam sed metus convallis, vehicula neque
eu, porttitor augue.

Suspendisse nulla ex, laoreet lacinia ante sit amet, convallis fermentum ligula.
Mauris at mauris lacinia, suscipit est ut, pulvinar tellus. Quisque fermentum
lectus leo, quis feugiat ante egestas eu. Praesent sollicitudin massa in sapien
consectetur blandit. Aenean semper id magna imperdiet varius. Etiam quis augue a
augue tincidunt mollis sit amet faucibus metus. Vivamus a arcu efficitur,
tristique mauris a, dapibus tellus. Lorem ipsum dolor sit amet, consectetur
adipiscing elit. Integer rutrum, tellus eget dapibus finibus, erat elit tempor
magna, eu molestie turpis nunc a erat. Aenean et venenatis felis, commodo
blandit eros. Proin cursus, leo eu consectetur suscipit, mauris nunc interdum
nibh, ut convallis velit elit vel elit. Cras rutrum justo a molestie pulvinar.
Praesent sed dolor justo. Donec malesuada eros in ipsum rutrum, ut rutrum arcu
varius.

In est neque, tempus sit amet lacus quis, facilisis mattis nunc. In porttitor
eros sed varius consectetur. Mauris ultrices facilisis pellentesque. Fusce quis
risus arcu. Phasellus id lorem vel nisl fringilla bibendum eget a ex. Etiam non
eros et lectus ultrices commodo sed eu lectus. Proin nec nulla pharetra,
convallis odio id, rutrum nulla. Maecenas laoreet volutpat ipsum, quis gravida
urna blandit et. Donec at interdum lorem, et fermentum magna. Integer feugiat
justo vitae orci luctus, a rutrum ligula consequat. Pellentesque egestas a elit
eu aliquam. Curabitur sollicitudin lacus erat, id dictum arcu congue non.
Aliquam laoreet, ligula non pellentesque pretium, magna magna egestas risus, in
scelerisque nisi sem a neque. Aenean et odio quis mauris lobortis consectetur.
Vestibulum in ornare quam, sed tempor orci. 
