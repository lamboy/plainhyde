# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "plainhyde"
  spec.version       = "0.2.0"
  spec.authors       = ["Jan Lamboy"]
  spec.email         = ["hallo@lamboy.net"]

  spec.summary       = "a simple jekyll theme"
  spec.homepage      = "https://gitlab.com/lamboy/plainhyde"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_layouts|_includes|_sass|LICENSE|README|_config\.yml|index\.md)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.1"
end
